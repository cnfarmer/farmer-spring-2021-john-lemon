﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonBehavior : MonoBehaviour
{
    public void easyClicked() 
    {
        SceneManager.LoadScene("EasyScene");
    }

    public void hardClicked()
    {
        SceneManager.LoadScene("HardScene");
    }

    public void instructionsClicked() 
    {
        SceneManager.LoadScene("Instructions");
    }

    public void backClicked()
    {
        SceneManager.LoadScene("Menu");
    }
}
